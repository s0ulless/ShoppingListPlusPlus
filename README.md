Description
===========
ShoppingListPlusPlus is the companion Android app for the Udacity course Firebase Essentials : Build a Collaborative Shopping List App. https://www.udacity.com/course/firebase-essentials-for-android--ud009

The course was created before the release of Firebase 3.

The version here is based on the 5.15 version at the official course repository at https://github.com/udacity/ShoppingListPlusPlus/tree/5.15_finish_the_rules-solution. The only difference is that I've updated it to use Firebase 3.


Usage:
======
1. Download the files and import it into your Android Studio.

2. Download the google-services.json config file from your ShoppingList++ Firebase console and place it in the app folder.
    - You may also want to check your SHA1 certificate fingerprints in Firebase.

3. Update UniqueFirebaseRootUrl and UniqueWebAppClientID in gradle.properties file.
    - UniqueFirebaseRootUrl is the location of your Firebase database.
    - UniqueWebAppClientID is your OAuth 2.0 client ID for web application.

4. Update your Firebase rules to match those at https://github.com/udacity/ShoppingListPlusPlus/blob/b805eb0a9dfef7486fd6aad3fa4f49f84d2211b0/rules/rules.json, if you haven't done so.


Changes:
========
1. The login flow for signups using e-mail and password has been changed. Instead of sending new users the password reset e-mail, the verification e-mail is sent. As such the 'hasLoggedInWithPassword' property is not used.

2. I am handling authenticated users in the 'onCreate' method in the LoginActivity class. However, I do not know if this should be the way.

3. Do take note of the sdk, build tool and dependencies versions in the build.gradle(app).

4. The changes made are not necessarily correct or the best way to do so. It is just a temporary fix till the official course updates. Should you have a proper fix or if there is already an official update, I would appreciate very much if you could share them. Thank you. :)


Reference Links:
================
1. Updating Firebase: https://firebase.google.com/support/guides/firebase-android

2. Generating SHA-1 fingerprint in Android Studio: http://stackoverflow.com/questions/27609442/how-to-get-the-sha-1-fingerprint-certificate-in-android-studio-for-debug-mode

3. FirebaseUI fix: https://github.com/firebase/FirebaseUI-Android/issues/392


Credits:
========
All credits to Udacity and the Google team that created the 'Firebase Essentials for Android' course.